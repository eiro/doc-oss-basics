* la plupart des projets sont abandonnés en cours de route
* si tu penses avoir une meilleure idée (architecturalement, techniquement),
  fais part de ton idée aux développeurs des projets existants.
* repartir de zero est souvent une mauvaise idée: même si tu finis le code,
  combien de temps mettra-tu à construire une communauté, maintenir la
  doc, faire de la promo ... et toutes ces choses non-techniques qui font
  la différence entre un projet libre et un bon projet libre.


