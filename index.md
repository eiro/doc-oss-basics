# que?

* vous voulez contribuer et vous ne savez pas par ou commencer ?
* votre projet ne décolle pas ?

nous structurons ici les bonnes idées a mettre en pratique. merci de

## alors comme ca tu veux te lancer ? sache que ...

### un mauvais code (ne fusse-t'il pas le tien) est meilleur que deux mauvais codes

Avant de commencer à coder, ne sous-estime jamais la valeur d'un logiciel et
mesure les conséquences de la balkanisation des projets libres. Sache aussi que
beaucoup de bases de codes étaient très belles au début (encore plus belles
lorsqu'elles n'étaient que sur le papier) et que nous n'avons

* effectue une [carto.md](cartographie des logiciels libres) qui proposent des
  fonctionnalités similaires aux tiennes. contacte les projets qui te
  paraissent les plus pertinents et intègre ces communautés et tente de
  discuter avec leurs membres: ils ont pe des visions similaires aux tiennes
  et ne manquaient pe que de tes bras pour aller dans le sens que tu souhaites.
  Tente d'entrer en contact avec ceux qui développent ce projet et tente de
  créer une [discution_constructive.md](discution constructive) et abstient toi
  de commencer ton propre projet pour de [fausse_bonne_idee.md](fausses bonnes idées).

* le nombre de projets libres qui sont morts parcequ'ils avaient raison
  (architecturalement, fonctionnellement, ...). Il vaut mieux privilégier une
  communauté qui a raison éthiquement et fonctionnellement qu'une communauté
  d'élitistes techniques. les logiciels qui ont raison contre le reste ne
  décollent pas (cf. plan9).

* suis ta voie quoi qu'il arrive: une fois que tu connais vraiment une
  communauté, que tu connais ses problèmes, ses forces, et que tu penses qu'une
  autre voie est possible, alors il te faut suivre ta voie et lancer ton projet
  (eventuellement en forkant).

### un mauvais code est meilleur que pas de code du tout

tu as un bout de code qui fait un truc inédit? tu veux créer qqchose qui
n'existe pas (ou qui n'a pas d'alternative libre?) il est temps de créer ton
projet!

* [bien commencer son projet](goude_practices.md)

* créer un fichier readme (préférablement du texte plein éventuellement
      formaté en markdown) à la racine. ce fichier décrit a minima
        * ce que vous tentez de réaliser avec ce projet
        * l'état du projet
        * les canaux à utiliser pour obtenir de l'assistance,
          faire des retours d'usages et/ou contribuer.

### qualité minimale

sache que pour un utilisateur comme pour un contributeurs, la rencontre
avec ton projet est un moment capitale pour son adoption.










