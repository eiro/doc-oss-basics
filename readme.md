# Audit de ton logiciel libre

la lecture du présent dépot commence [ici](index.md)

## Todo

références et docs à trouver sur le logiciel libre

## Idées pour amener à la contribution

* sensibiliser aux bonnes pratiques (reuse, test, revision, review, document)
* faire des bases d'exercices dont les derniers niveaux sont des contributions
  a des projets libres.

* liste des petites missions des projets. faire pour l'ESR et Framasoft
  un [https://github.com/MunGell/awesome-for-beginners](awesome for beginners),
  contacter ce gentil monsieur MunGell.
